<?php

/*
 * This file is part of the yangzhuang/dindging.
 *
 * (c) 杨壮 
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace EasyDingTalk\Smart;

use EasyDingTalk\Kernel\BaseClient;

class Client extends BaseClient
{
    /**
     * 获取在职员工列表
     *
     * @param string $status_list 员工状态，可以查询多个状态。不同状态之间使用英文逗号分隔。2：试用期3：正式5：待离职-1：无状态
     *  @param int $offset 分页 第几页
     * @param int $size 获取条数（最多50条）
     * @return mixed
     */
    public function getStaffList($status_list,$offset = 0,$size = 40)
    {
        return $this->client->postJson('topapi/smartwork/hrm/employee/queryonjob', ['status_list' => $status_list ,'offset' => $offset,'size' => $size,]);
    }

    /**
     * 获取待入职员工列表
     *  @param int $offset 分页 第几页
     * @param int $size 获取条数（最多50条）
     * @return mixed
     */
    public function getEmployeeUserIds($offset = 0,$size = 40)
    {
        return $this->client->postJson('topapi/smartwork/hrm/employee/querypreentry', ['offset' => $offset,'size' => $size,]);
    }

    /**
     * 获取员工离职信息
     * @param int $userid_id 员工ID
     * @return mixed
     */
    public function getQuitListUserList($userid_id)
    {

        return $this->client->postJson('topapi/smartwork/hrm/employee/listdimission', ['userid_list' => $userid_id]);
    }

    /**
     * 获取离职员工列表
     * @param int $offset
     * @param int $size
     * @return mixed
     */
    public function getDimissionUserIds($offset = 0,$size = 40)
    {
        return $this->client->postJson('topapi/smartwork/hrm/employee/querydimission', [ 'offset' => $offset,'size' => $size,]);
    }


    /**
     * 获取员工花名册字段信息
     * @param int $userid_id
     */
    public function getUserList($userid_id)
    {
        return $this->client->postJson('topapi/smartwork/hrm/employee/list', ['userid_list' => $userid_id]);
    }

    /**
     * 获取员工花名册字段信息(v2)
     * @param $userid_id
     * @param $agentid
     * @return mixed
     */
    public function getUserListV2($userid_id,$agentid)
    {
        return $this->client->postJson('topapi/smartwork/hrm/employee/v2/list', ['userid_list' => $userid_id , 'agentid' => $agentid ]);
    }


    /**
     * 获取花名册元数据
     * @param $agentid
     * @return mixed
     */
    public function getRosterMeta($agentid)
    {
        return $this->client->postJson('topapi/smartwork/hrm/roster/meta/get', ['agentid' => $agentid ]);
    }

}
